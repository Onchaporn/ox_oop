/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox_oop;

/**
 *
 * @author Dream World
 */
public class Board {
   char[][] table = {
			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'}
	};
	
	private Player X;
	private Player O;
	private Player winner;
	private Player current;
	private int turnCount;
	

        public Board(Player X, Player O) {
                this.X = X;
		this.O = O;
		current = X;
		winner = null;
		turnCount = 0;
        }


	public char [][] getTable(){
		return table;
	}
	
	public Player getCurrentPlayer() {
		return current;
	}
	
	public boolean setTable(int row,int col) {
		if(table[row][col]=='-') {
			table[row][col] = current.getName();
			return true;
		}
		return false;
	}
	
	private boolean checkRow(int row) {
		for(int col = 0; col<table.length; col++) {
			if(table[row][col]!= current.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkRow() {
		if(checkRow(0)||checkRow(1)||checkRow(2)) {
			return true;
		}
		return false;
	}
	
	private boolean checkCol(int col) {
		for(int row = 0; row<table.length; row++) {
			if(table[row][col]!= current.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkCol() {
		if(checkCol(0)||checkCol(1)||checkCol(2)) {
			return true;
		}
		return false;
	}
	
	private boolean checkx1() {
		for(int i = 0; i<table.length; i++) {
			if(table[i][i]!= current.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkx2() {
		for(int i = 0; i<table.length; i++) {
			if(table[2-i][i]!= current.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkDraw() {
		if(turnCount == 8) {
			X.draw();
			O.draw();
			return true;
		}
		return false;
	}
	
	public boolean checkWinner() {
		if(checkRow() || checkCol() || checkx1() || checkx2()) {
			winner =current;
			if(current == X) {
				X.winner();
				O.lose();
			}else {
				O.winner();
				X.lose();
			}
			return true;
		}
		return false;
		
	}
	
	public boolean isFinish() {
		if(checkWinner()) {
			return true;
		}
		if(checkDraw()) {
			return true;
		}
		
		return false;
	}
	
	public void switchPlayer() {
		if(current == X) {
			current = O;
		}else {
			current = X;
		}
		turnCount++;
	}
	public Player getWinner() {
		return winner;
	}

}
