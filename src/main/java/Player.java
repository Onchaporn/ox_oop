/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dream World
 */
public class Player {
    private char name;
	private int winner;
	private int draw;
	private int lose;
	
	public Player(char name){
		this.name = name;
		winner = 0;
		draw = 0;
		lose = 0;
	}
public char getName() {
		return name;
	}
	
	public int getWinner() {
		return winner;
	}
	
	public int getDraw() {
		return draw;
	}
	
	public int getLose() {
		return lose;
	}
	
	public void win() {
		winner++;
	}
	
	public void draw() {
		draw++;
	}
	
	public void lose() {
		lose++;
	}
	
	
}


